![alt txt](http://spinorlab.matrix.jp/en/wp-content/uploads/2017/12/Connecting-.ZERO-to-the-PC-900x570.jpg)

```
$ python3 aa30zero_cli_part1.py 5200000 1000000 100 | tee touchstone.s1p
  
$ cat touchstone.s1p
!! freq =  5200000
!! bw =  1000000
!! ndiv =  100
!!  Serial<id=0x10fab2860, open=True>(port='/dev/cu.usbmodem14701', baudrate=38400, bytesize=8, parity='N', stopbits=1, timeout=1, xonxoff=False, rtscts=False, dsrdtr=False)
!!  b'AA-30 ZERO 150\r\n'
!!  b'OK\r\n'
!!  b'OK\r\n'
# MHz S RI R 50
!  4.7 26.910389 -45.76308
4.7 0.0397565169109296 -0.5713623335864779
...

$ python3 aa30zero_cli_part2.py
```

![alt txt](http://spinorlab.matrix.jp/en/wp-content/uploads/2018/12/Screen-Shot-2018-12-02-at-15.36.34.png)