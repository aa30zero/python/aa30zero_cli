
# aa30zero_cli_part2.py
# read a touchstone file and plot
 
import matplotlib.pyplot as plt
import skrf as rf
from pylab import xkcd
import numpy as np
 
xkcd()
plt.style.use('ggplot')
plt.figure(figsize=(8, 18))
 
nwk = rf.Network('touchstone.s1p')
 
index = np.argmin(nwk.s_db)
fpeak = nwk.f[index] / 1000000 # fvector in Hz
rlmax = nwk.s_db[index, 0, 0]
 
plt.subplot(4, 1, 1)
nwk.plot_s_db(label='S11 [dB]', lw=3)
plt.title('Return Loss max = {0:.2f} dB at {1} [MHz]'.format(-rlmax, fpeak))
plt.grid(True)
 
plt.subplot(4, 1, 2)
nwk.plot_z_re(label='Real{Z}', color='blue', lw=3)
plt.grid(True)
 
plt.subplot(4, 1, 3)
nwk.plot_z_im(label='Imag{Z}', color='orange', lw=3)
plt.grid(True)
 
plt.subplot(4, 1, 4)
nwk.plot_s_smith(label='S11', draw_labels=True, color='green', lw=3)
 
plt.show()