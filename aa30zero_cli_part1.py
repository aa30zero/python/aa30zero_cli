# aa30zero_cli_part1.py
# generate a touchstone file
 
import serial
import time
import argparse
 
parser = argparse.ArgumentParser()
parser.add_argument("freq", help="center freq in kHz, example 7000000")
parser.add_argument("bw", help="band width in kHz, example 2000000")
parser.add_argument("ndiv", help="no. of divisions, example 100")
 
args = parser.parse_args()
print('!! freq = ', args.freq)
print('!! bw = ', args.bw)
print('!! ndiv = ', args.ndiv)
 
ser = serial.Serial('/dev/cu.usbmodem14701', 38400, timeout=1)
print('!! ', ser)
 
time.sleep(2)
ser.write(b'ver'+b'\x0a')
 
time.sleep(1)
ser.write(b'fq'+args.freq.encode('ASCII')+b'\x0a')
 
time.sleep(1)
ser.write(b'sw'+args.bw.encode('ASCII')+b'\x0a')
 
for i in range(3):
    line = ser.readline()
    print('!! ', line)
 
time.sleep(1)
ser.write(b'frx'+args.ndiv.encode('ASCII')+b'\x0a')
 
print('# MHz S RI R 50')
z0 = complex(50.0, 0.0)
 
for i in range(int(args.ndiv)+1):
    line = ser.readline().decode(encoding='utf-8').rstrip().split(',')
    freq = float(line[0])
    z = complex(float(line[1]), float(line[2]))
    rho = (z-z0)/(z+z0)
    print('! ', freq, z.real, z.imag)
    print(freq, rho.real, rho.imag)
 
for i in range(1):
    line = ser.readline()
    print('!! ', line)
 
ser.close()